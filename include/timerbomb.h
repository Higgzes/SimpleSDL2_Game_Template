#pragma once
#include "common.h"


class TimerBomb
{
    uint16_t point_radius;
    uint16_t damage_radius;
    unsigned int pre_explosion_time {1300};
    uint16_t damage_strength {50};
    unsigned int time_created;
    unsigned int start_explosion_time;
    SDL_Point position;
    uint16_t current_radius {0};
    uint16_t explosion_speed {100};

public:
    std::unordered_set<uint16_t> damaged_obj_ids;

    TimerBomb(SDL_Point, uint16_t, uint16_t);
    bool time_to_blowUp() const;
    enum status {PRE_EXPLOSION, EXPLOSION_PROCESS, EXPIRED} current_status;
    void set_start_explosion_time (unsigned int time) {start_explosion_time = time;}
    void update_current_explosion_radius ();
    auto get_current_explosion_radius () const {return current_radius;}
    auto get_damage_radius () const {return damage_radius;}
    auto get_point_radius () const {return point_radius;}
    auto get_position () const {return position;}
    auto get_damage_value () const {return damage_strength;}

};



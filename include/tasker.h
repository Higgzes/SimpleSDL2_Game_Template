#include "object.h"
#include "house.h"

class Tasker final
{
private:
    bool map[WINDOW_WIDTH*WINDOW_HEIGHT];

public:
    Tasker() noexcept;
    void update_map(const std::vector<Object>&, const House&);
    auto getMap () const {return &map[0];}
    std::unordered_map<uint16_t, SDL_Point> find_where_to_place (const std::vector<Object>&, const House&, SDL_Point) const;
    SDL_Point calc_objs_pos(const Object&);
    SDL_Point find_where_to_place_new_obj(const SDL_Rect&, const House&, const std::vector<Object>&) const;
};

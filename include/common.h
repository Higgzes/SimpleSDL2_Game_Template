#pragma once
#include <iostream>
#include <optional>
#include <vector>
#include <algorithm>
#include <numbers>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <chrono>
#include <queue>
#include <array>
#include <thread>
#include <mutex>
#include <execution>
#include <future>
#include <fstream>
#include <random>

#include <SDL2/SDL.h>
#include "sdlStbFont.h"

#include "Pathfinder/triangle_lib_navmesh.h"
#include "Pathfinder/behaviorBuilder.h"
#include "Pathfinder/navmesh_interface.h"
#include "Pathfinder/pathfinder.h"
#include "Pathfinder/vector.h"

inline constexpr uint16_t WINDOW_WIDTH = 1200;
inline constexpr uint16_t WINDOW_HEIGHT = 800;
inline constexpr uint16_t MAX_RADIUS = std::max(WINDOW_WIDTH, WINDOW_HEIGHT)*std::numbers::sqrt2_v<float>;
inline constexpr uint16_t MAX_DIM = std::max(WINDOW_WIDTH, WINDOW_HEIGHT);

inline bool operator==(SDL_Point const &a, SDL_Point const &b)
{
    return a.x == b.x && a.y == b.y;
}

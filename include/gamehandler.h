#pragma once
#include "input.h"

class GameHandler final {
public:
        SDL_Window *sdlWindow_;
        SDL_Renderer *sdlRenderer_;

        Input user_action;
        GameManager game_process;

        GameHandler();
        ~GameHandler();
        bool isGameOver = false;
        void input();
        void update();
        void render();
};


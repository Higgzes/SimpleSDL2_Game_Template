#pragma once
#include "baseobject.h"

class Diamond : public BaseObject
{

    unsigned int time_created;
    std::array<SDL_Vertex, 4> DiamondVerticies;
    std::array<int, 6> DiamondindexList {0,1,2,0,2,3};

public:
    Diamond(SDL_Point point, std::pair<uint16_t, uint16_t> dim, SDL_Color color = {74, 237, 216, 255});
    Diamond(const Diamond& d) = default;
    Diamond(Diamond&&) = default;
    Diamond& operator=(Diamond&&) = default;
    ~Diamond() = default;

    SDL_Color getColor() const override {return color;}
    unsigned int get_time() const;

    int getDiamondIndexSize () const {return DiamondindexList.size();}
    const int* getDiamondIndexList () const {return DiamondindexList.data();}
    int getDiamondVerticiesSize () const {return DiamondVerticies.size();}
    const SDL_Vertex* getDiamondVerticies () const {return DiamondVerticies.data();}
};


#pragma once
#include "object.h"
#include "house.h"
#include "tasker.h"
#include "diamond.h"
#include "timerbomb.h"

class GameManager final
{
    std::vector<Object> objs;
    std::vector<Diamond> diamonds;
    std::vector<TimerBomb> bombs;
    House base;
    SDL_Rect pre_obj;
    uint16_t targeted_objID = 0;
    uint16_t current_game_process {0};
    const uint16_t need_to_reach = {9200};

    struct selection {
    std::optional<SDL_Rect> selected_area;
    SDL_Point selection_start, selection_end;
    bool isSelectionProcess = false;
    } selection;
    const uint8_t frame_w = 3;

    Tasker move_processing, new_obj_processing;
    std::unordered_map<uint16_t, std::unique_ptr<std::jthread>> unstuck_thr;
    std::unordered_map<uint16_t, SDL_Point> unstuck_results;
    bool process_stuck_obj;

    SDL_Renderer* renderer;
    sttfont_memory mem;

    uint32_t balance {10}; //10 - default

    struct unit_spec {
        uint8_t price;
        uint16_t w;
        uint16_t h;
        uint16_t speed;
        uint16_t health;
    } small_obj{10, 10, 20, 140, 16}, med_obj{30, 32, 24, 130, 180}, large_obj{80, 40, 40, 50, 1000};

    struct diamond_spec {
        uint16_t w;
        uint16_t h;
        uint16_t value;
    } small_diamond{16, 32, 5};

    struct bomb_spec {
        uint16_t damage_radius;
        uint16_t point_radius;
    } small_bomb{250, 5};

    SDL_Color non_selected {255,255,255,255}, selected {200,200,200,255};
    bool is_enought_money_to_buy {false};

    uint32_t time_since_last_diamond_created;
    uint32_t diamond_appear_interval {300}; //300 - default
    unsigned int diamond_appear_chance {3}; //3 - default
    uint16_t diamond_max_size {5}; //5 - default
    const unsigned int diamond_limit_time = 3800; //3800 - default

    uint32_t time_since_last_bomb_created;
    uint32_t bomb_appear_interval {100}; //100 - default
    unsigned int bomb_appear_chance {2}; //2 - default
    uint16_t bomb_max_size {4}; //4 - default

    std::random_device os_seed;
    bool need_to_create_new_diamond ();
    SDL_Point get_new_diamond_pos ();
    bool need_to_create_new_bomb ();
    SDL_Point get_new_bomb_pos();

public:

    GameManager();
    enum Status {ACTIVE, GAMEOVER_WON, GAMEOVER_LOST} gameStatus;
    std::optional<uint16_t> isInsideObj(const SDL_Point) const;
    bool isAnyObjSelected() const;
    void add_obj ();
    void render();
    void clear_selection();
    bool selection_process() const {return selection.isSelectionProcess;}
    void start_select_area(const SDL_Point);
    SDL_Point get_selection_endPoint() const {return selection.selection_end;}
    void set_selection_endPoint(const SDL_Point);
    void select_obj ();
    void select_obj (uint16_t);
    void end_select_area();
    void find_place_for_obj(const SDL_Point);
    void process_updates();
    void setRenderer(SDL_Renderer*);
    void init_font(const std::string&);
    bool isFocusedOnBaseSlots(const SDL_Point) const;
    void process_popup();
};


#pragma once
#include "common.h"

class BaseObject
{
protected:
    static uint16_t current_id;
    uint16_t id;

public:

    bool isSelected = false;
    SDL_Rect rect;
    SDL_Color color;

    virtual ~BaseObject() = default;
    uint16_t getID() const {return id;}
    virtual SDL_Color getColor() const = 0;
};


#pragma once
#include "baseobject.h"

class Object : public BaseObject
{
    inline static std::mutex mu;

    uint16_t current_health;
    uint16_t max_health;
    uint16_t speed;

    struct path {
        SDL_Point start_p = {-1, -1};
        SDL_Point goal_p = {-1, -1};
        SDL_Point destination_p = {-1, -1};
        std::vector<std::pair<SDL_Point, SDL_Point>> segments;
        Uint64 prevTicks;
        uint16_t current_segment;

        std::vector<std::pair<float, SDL_Point>> distances;
    } path;

    std::shared_ptr<pathfinder::navmesh::NavmeshInterface> navmesh;
    bool border_mask_c[WINDOW_WIDTH*WINDOW_HEIGHT];
    char area_available_c[WINDOW_HEIGHT][WINDOW_WIDTH];

    void fill_area_alg (int16_t, int16_t);
    bool createNavmesh(const std::vector<Object>&, const SDL_Rect&);
    void createMask (const bool*);
    void writePoly(const auto&, std::string);

public:

    SDL_Rect rect_dest_mask;

    bool hasPath = false;
    enum status {STATIC,
                 NEED_TO_BUILD_PATH,
                 NEED_TO_BUILD_PATH_FOR_ALT_DEST,
                 NEED_TO_FIND_ALT_DEST,
                 MOVING,
                 DEST_ACHIEVED,
                 STUCK} current_status;

    Object(int x, int y, int w, int h, uint16_t speed, uint16_t health, SDL_Color color = {255, 0, 0, 255});

    std::optional<SDL_Point> getDestination () const;
    std::vector<std::pair<SDL_Point, SDL_Point>>  getPathSegmnets ()  const {return path.segments;}
    SDL_Point getGoalPoint () const {return path.goal_p;}

    bool operator == (const Object&) const;

    SDL_Color getColor() const override;
    SDL_Point getPos() const {return {rect.x+rect.w/2, rect.y+rect.h/2};}

    void setDest(const SDL_Point);
    void setPos (const SDL_Point);
    void setGoal (const SDL_Point);
    void buildPath (const std::vector<Object>&, const SDL_Rect&);
    void flood_fill (const bool*);
    bool find_alt_dest();
    void add_new_mask (const std::vector<SDL_Rect>&);
    Uint64 getPrevTicks () const {return path.prevTicks;}
    uint16_t getSpeed() const {return speed;}
    uint16_t getCurrentSegment() const {return path.current_segment;}
    void process_next_segment();
    bool is_possible_to_unstuck () const;
    auto get_current_health () const {return current_health;}
    void set_current_health (uint16_t value) {current_health = value;}
    auto get_max_health () const {return max_health;}
};


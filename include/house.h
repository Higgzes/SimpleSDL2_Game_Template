#pragma once
#include "baseobject.h"

class House : public BaseObject
{
    sttfont_formatted_text balance_formtxt;
    struct labels {
        SDL_Texture* texture = nullptr;
        SDL_Rect rectTexture;
        sdl_stb_font_cache fc;
        SDL_Point endTexturePoint;
    } balance, price_small, price_med, price_large, no_money, progress;

    SDL_Rect small_slot, med_slot, large_slot;
    SDL_Rect small_sample, med_sample, large_sample;

    std::vector<SDL_Vertex> DiamondVerticies;
    const std::array<int, 24> DiamondindexListUnfocused {0,1,4,4,1,5,1,5,6,2,6,1,2,6,3,3,7,6,3,7,4,0,4,3};
    const std::array<int, 6> DiamondindexListFocused {0,1,2,0,2,3};

public:
    enum UnitSize {SMALL, MEDIUM, LARGE};
    mutable std::optional<UnitSize> lastFocused = {};
    House(int x, int y, int w = 100, int h = 160, SDL_Color color = {255, 0, 0, 255});
    House(const House& d) = default;
    House(House&&) = default;
    House& operator=(House&&) = delete;
    ~House() = default;
    SDL_Color getColor() const override {return color;}
    void processFont(sttfont_memory&, SDL_Renderer*, std::array<uint8_t, 3>);
    SDL_Rect* getRectTexture_balance ();
    SDL_Rect* getRectProgress_value ();
    SDL_Texture* getTexture_balance ();
    SDL_Texture* getProgress_value ();
    SDL_Rect* getRectTexture_noMoney ();
    SDL_Texture* getTexture_noMoney ();
    void updateBalance(uint32_t);
    void updateProgress(double);
    SDL_Rect* getRectPriceTexture (UnitSize);
    SDL_Texture* getPriceTexture (UnitSize);
    int getDiamondIndexSize () const;
    const int* getDiamondIndexList ();
    int getDiamondVerticiesSize () {return DiamondVerticies.size();}
    SDL_Vertex* getDiamondVerticies ();
    SDL_Rect* getSlot_Rect (UnitSize);
    SDL_Rect* getSample_Rect (UnitSize);
    bool isPointInsideSlots (SDL_Point) const;
    SDL_Point getDiamondMiddlePoint () const;
};


#include "diamond.h"

Diamond::Diamond(SDL_Point point, std::pair<uint16_t, uint16_t> dim, SDL_Color color)
    : time_created(SDL_GetTicks())
{
    id = BaseObject::current_id++;
    this->color = color;
    rect.x = point.x;
    rect.y = point.y;
    rect.w = dim.first;
    rect.h = dim.second;
    SDL_Vertex a = {{static_cast<float>(rect.x+rect.w/2),static_cast<float>(rect.y)},color};
    SDL_Vertex b = {{static_cast<float>(rect.x+rect.w), static_cast<float>(rect.y+rect.h/2)},color};
    SDL_Vertex c = {{static_cast<float>(rect.x+rect.w/2),static_cast<float>(rect.y+rect.h)},color};
    SDL_Vertex d = {{static_cast<float>(rect.x),static_cast<float>(rect.y+rect.h/2)},color};
    DiamondVerticies = {a, b, c, d};
}

unsigned int Diamond::get_time() const {
    return SDL_GetTicks()-time_created;
}



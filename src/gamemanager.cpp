#include "gamemanager.h"
#define SDL_STB_FONT_IMPL
#include "sdlStbFont.h"

uint16_t BaseObject::current_id{0};

GameManager::GameManager() : base(50, WINDOW_HEIGHT/2) {
   time_since_last_diamond_created = SDL_GetTicks();
   time_since_last_bomb_created = SDL_GetTicks();
   gameStatus = GameManager::ACTIVE;
}

std::optional<uint16_t> GameManager::isInsideObj(SDL_Point point) const {
    for (auto& obj : objs)
        if (SDL_PointInRect(&point, &obj.rect)) return obj.getID();
    return {};
}

bool GameManager::isAnyObjSelected() const {
    return std::any_of(objs.begin(), objs.end(), [](const Object& o) {return o.isSelected;});
}

void GameManager::add_obj() {
    uint16_t speed, health;
    if (!is_enought_money_to_buy) return;
    if (pre_obj.w == small_obj.w && pre_obj.h == small_obj.h) {
        balance -= small_obj.price;
        speed = small_obj.speed;
        health = small_obj.health;
    } else if (pre_obj.w == med_obj.w && pre_obj.h == med_obj.h) {
        balance -= med_obj.price;
        speed = med_obj.speed;
        health = med_obj.health;
    } else {
        balance -= large_obj.price;
        speed = large_obj.speed;
        health = large_obj.health;
    }
    objs.emplace_back(pre_obj.x+pre_obj.w/2, pre_obj.y+pre_obj.h/2, pre_obj.w, pre_obj.h, speed, health);
}

void GameManager::render()  {
    if (gameStatus != Status::ACTIVE) {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_Rect background {WINDOW_WIDTH/4,WINDOW_HEIGHT/4,WINDOW_WIDTH/2,WINDOW_WIDTH/2};
        SDL_RenderFillRect(renderer, &background);

        SDL_Texture* textureGamestatus = nullptr;

        SDL_Point endPGamestatus;

        sdl_stb_font_cache fcGamestatus;
        fcGamestatus.faceSize = 35;
        fcGamestatus.loadFontManaged(mem);
        fcGamestatus.bindRenderer(renderer);
        std::string game_status_str = gameStatus == Status::GAMEOVER_LOST ?
            "        GAME OVER\n        YOU LOST\nnot enought money to buy any unit" :
            "        GAME OVER\n        YOU WON\n      100% reached";
        textureGamestatus = fcGamestatus.renderTextToTexture(game_status_str.c_str(), game_status_str.size(),
                                                             &endPGamestatus.x, &endPGamestatus.y);
        SDL_Rect rectTextureGamestatus {WINDOW_WIDTH/4+40,WINDOW_HEIGHT/4+40, endPGamestatus.x, endPGamestatus.y};
        SDL_RenderCopy(renderer, textureGamestatus, NULL, &rectTextureGamestatus);
        SDL_DestroyTexture(textureGamestatus);

        return;
    }
///render_red_base
    SDL_SetRenderDrawColor(renderer, base.color.r, base.color.g, base.color.b, base.color.a);
    SDL_RenderFillRect(renderer, &base.rect);

    SDL_Color small_Unit_Slot_col = (base.lastFocused.has_value() && base.lastFocused == House::SMALL) ?
                                        selected : non_selected;
    SDL_Color med_Unit_Slot_col = (base.lastFocused.has_value() && base.lastFocused == House::MEDIUM) ?
                                      selected : non_selected;
    SDL_Color large_Unit_Slot_col = (base.lastFocused.has_value() && base.lastFocused == House::LARGE) ?
                                        selected : non_selected;

    SDL_SetRenderDrawColor(renderer, small_Unit_Slot_col.r,
                                     small_Unit_Slot_col.g,
                                     small_Unit_Slot_col.b,
                                     small_Unit_Slot_col.a);
    SDL_RenderFillRect(renderer, base.getSlot_Rect(House::SMALL));

    SDL_SetRenderDrawColor(renderer, med_Unit_Slot_col.r,
                                     med_Unit_Slot_col.g,
                                     med_Unit_Slot_col.b,
                                     med_Unit_Slot_col.a);
    SDL_RenderFillRect(renderer, base.getSlot_Rect(House::MEDIUM));

    SDL_SetRenderDrawColor(renderer, large_Unit_Slot_col.r,
                                     large_Unit_Slot_col.g,
                                     large_Unit_Slot_col.b,
                                     large_Unit_Slot_col.a);
    SDL_RenderFillRect(renderer, base.getSlot_Rect(House::LARGE));

    SDL_SetRenderDrawColor(renderer, base.color.r, base.color.g, base.color.b, base.color.a);
    SDL_RenderFillRect(renderer, base.getSample_Rect(House::SMALL));
    SDL_RenderFillRect(renderer, base.getSample_Rect(House::MEDIUM));
    SDL_RenderFillRect(renderer, base.getSample_Rect(House::LARGE));

    SDL_RenderGeometry(renderer, nullptr, base.getDiamondVerticies(), base.getDiamondVerticiesSize(),
                           base.getDiamondIndexList(), base.getDiamondIndexSize());

    if (base.lastFocused.has_value()) {
        if (is_enought_money_to_buy) {
            SDL_Rect temp_sample;
            switch (base.lastFocused.value()) {
            case House::SMALL:
                temp_sample = {base.getDiamondMiddlePoint().x-small_obj.w/2,
                               base.getDiamondMiddlePoint().y-small_obj.h/2,
                               small_obj.w,small_obj.h};
                break;
            case House::MEDIUM:
                temp_sample = {base.getDiamondMiddlePoint().x-med_obj.w/2,
                               base.getDiamondMiddlePoint().y-med_obj.h/2,
                               med_obj.w,med_obj.h};
                break;
            case House::LARGE:
                temp_sample = {base.getDiamondMiddlePoint().x-large_obj.w/2,
                               base.getDiamondMiddlePoint().y-large_obj.h/2,
                               large_obj.w,large_obj.h};
                break;
            }
            SDL_SetRenderDrawColor(renderer, base.color.r, base.color.g, base.color.b, base.color.a);
            SDL_RenderFillRect(renderer, &temp_sample);

            SDL_Rect up_b{temp_sample.x, temp_sample.y, temp_sample.w, frame_w};
            SDL_Rect right_b{temp_sample.x+temp_sample.w-frame_w, temp_sample.y+frame_w, frame_w, temp_sample.h-frame_w};
            SDL_Rect left_b{temp_sample.x, temp_sample.y+frame_w, frame_w, temp_sample.h-frame_w};
            SDL_Rect down_b{temp_sample.x+frame_w, temp_sample.y+temp_sample.h-frame_w, temp_sample.w-2*frame_w, frame_w};

            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_RenderFillRect(renderer, &up_b);
            SDL_RenderFillRect(renderer, &right_b);
            SDL_RenderFillRect(renderer, &left_b);
            SDL_RenderFillRect(renderer, &down_b);

            SDL_SetRenderDrawColor(renderer, selected.r, selected.g, selected.b, selected.a);
            SDL_RenderFillRect(renderer, &pre_obj);

        } else {
            SDL_RenderCopy(renderer, base.getTexture_noMoney(), NULL, base.getRectTexture_noMoney());
        }
    }

    SDL_RenderCopy(renderer, base.getTexture_balance(), NULL, base.getRectTexture_balance());
    SDL_RenderCopy(renderer, base.getProgress_value(), NULL, base.getRectProgress_value());
    SDL_RenderCopy(renderer, base.getPriceTexture(House::SMALL), NULL, base.getRectPriceTexture(House::SMALL));
    SDL_RenderCopy(renderer, base.getPriceTexture(House::MEDIUM), NULL, base.getRectPriceTexture(House::MEDIUM));
    SDL_RenderCopy(renderer, base.getPriceTexture(House::LARGE), NULL, base.getRectPriceTexture(House::LARGE));    

///render_objects
    for (auto& obj : objs) {
        SDL_SetRenderDrawColor(renderer, obj.getColor().r, obj.getColor().g, obj.getColor().b, obj.getColor().a);
        SDL_RenderFillRect(renderer, &obj.rect);
        if (obj.isSelected) {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_Rect up_b{obj.rect.x, obj.rect.y, obj.rect.w, frame_w};
            SDL_Rect right_b{obj.rect.x+obj.rect.w-frame_w, obj.rect.y+frame_w, frame_w, obj.rect.h-frame_w};
            SDL_Rect left_b{obj.rect.x, obj.rect.y+frame_w, frame_w, obj.rect.h-frame_w};
            SDL_Rect down_b{obj.rect.x+frame_w, obj.rect.y+obj.rect.h-frame_w, obj.rect.w-2*frame_w, frame_w};
            SDL_RenderFillRect(renderer, &up_b);
            SDL_RenderFillRect(renderer, &right_b);
            SDL_RenderFillRect(renderer, &left_b);
            SDL_RenderFillRect(renderer, &down_b);
        }
        if (obj.isSelected && obj.current_status == Object::MOVING) {
            SDL_SetRenderDrawColor(renderer, obj.color.r, obj.color.g, obj.color.b, obj.color.a);
            SDL_RenderDrawLine(renderer, obj.getPos().x,
                                         obj.getPos().y,
                                         obj.getPathSegmnets().at(obj.getCurrentSegment()).second.x,
                                         obj.getPathSegmnets().at(obj.getCurrentSegment()).second.y);
            for (int i = obj.getCurrentSegment()+1; i< obj.getPathSegmnets().size(); i++)
                SDL_RenderDrawLine(renderer, obj.getPathSegmnets().at(i).first.x,
                                             obj.getPathSegmnets().at(i).first.y,
                                             obj.getPathSegmnets().at(i).second.x,
                                             obj.getPathSegmnets().at(i).second.y);
        }
    }
    if (selection_process() && selection.selected_area) {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderDrawLine(renderer, selection.selected_area.value().x, selection.selected_area.value().y,
                selection.selected_area.value().x+selection.selected_area.value().w, selection.selected_area.value().y);
        SDL_RenderDrawLine(renderer, selection.selected_area.value().x+selection.selected_area.value().w, selection.selected_area.value().y,
                selection.selected_area.value().x+selection.selected_area.value().w, selection.selected_area.value().y+selection.selected_area.value().h);
        SDL_RenderDrawLine(renderer, selection.selected_area.value().x+selection.selected_area.value().w, selection.selected_area.value().y+selection.selected_area.value().h,
                selection.selected_area.value().x, selection.selected_area.value().y+selection.selected_area.value().h);
        SDL_RenderDrawLine(renderer, selection.selected_area.value().x, selection.selected_area.value().y+selection.selected_area.value().h,
                selection.selected_area.value().x, selection.selected_area.value().y);
    }

///render_diamonds
    for (const auto& diamond : diamonds)
        SDL_RenderGeometry(renderer, nullptr, diamond.getDiamondVerticies(), diamond.getDiamondVerticiesSize(),
                           diamond.getDiamondIndexList(), diamond.getDiamondIndexSize());

///render_bomb_blinking_point
    for (const auto& bomb : bombs) {
        if (bomb.current_status == TimerBomb::PRE_EXPLOSION) {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            int x {bomb.get_position().x}, y {bomb.get_position().y};
            auto radius = bomb.get_point_radius();
            unsigned int offsetx {0}, offsety {radius}, d {offsety-1};
            int status{0};
            while (offsety >= offsetx) {

                status += SDL_RenderDrawLine(renderer, x - offsety, y + offsetx,
                                             x + offsety, y + offsetx);
                status += SDL_RenderDrawLine(renderer, x - offsetx, y + offsety,
                                             x + offsetx, y + offsety);
                status += SDL_RenderDrawLine(renderer, x - offsetx, y - offsety,
                                             x + offsetx, y - offsety);
                status += SDL_RenderDrawLine(renderer, x - offsety, y - offsetx,
                                             x + offsety, y - offsetx);
                if (d >= 2*offsetx) {
                    d -= 2*offsetx + 1;
                    offsetx +=1;
                }
                else if (d < 2 * (radius - offsety)) {
                    d += 2 * offsety - 1;
                    offsety -= 1;
                }
                else {
                    d += 2 * (offsety - offsetx - 1);
                    offsety -= 1;
                    offsetx += 1;
                }
            }
        } else if (bomb.current_status == TimerBomb::EXPLOSION_PROCESS) {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            auto r = bomb.get_current_explosion_radius();
            float i = 0.0f;
            int x {bomb.get_position().x}, y {bomb.get_position().y};
            while (i < 2*std::numbers::pi) {
                int dx = static_cast<int>(r * cos(i))+x;
                int dy = static_cast<int>(r * sin(i))+y;                             
                i += std::numbers::pi/r;
                SDL_Point temp_point {dx, dy};
                if (dy<0 || dy>WINDOW_HEIGHT || dx<0 || dx>WINDOW_WIDTH || SDL_PointInRect(&temp_point, &base.rect)) continue;
                SDL_RenderDrawPoint(renderer, dx, dy);
            }
        }
    }
}

void GameManager::clear_selection() {
    std::for_each(objs.begin(), objs.end(), [](Object& o){o.isSelected = false;});
}

void GameManager::start_select_area(SDL_Point point) {
    selection.isSelectionProcess = true;
    selection.selection_start = point;
    selection.selection_end = point;
}

void GameManager::set_selection_endPoint(SDL_Point point) {
    selection.selection_end = point;
    SDL_Rect temp_rect;
    if (point.x>=selection.selection_start.x && point.y>=selection.selection_start.y) {
        temp_rect.x = selection.selection_start.x;
        temp_rect.y = selection.selection_start.y;
        temp_rect.w = point.x-selection.selection_start.x;
        temp_rect.h = point.y-selection.selection_start.y;
    } else if (point.x>=selection.selection_start.x && point.y<selection.selection_start.y) {
        temp_rect.x = selection.selection_start.x;
        temp_rect.y = point.y;
        temp_rect.w = point.x-selection.selection_start.x;
        temp_rect.h = selection.selection_start.y-point.y;
    } else if (point.x<selection.selection_start.x && point.y>=selection.selection_start.y) {
        temp_rect.x = point.x;
        temp_rect.y = selection.selection_start.y;
        temp_rect.w = selection.selection_start.x-point.x;
        temp_rect.h = point.y-selection.selection_start.y;
    } else if (point.x<selection.selection_start.x && point.y<selection.selection_start.y) {
        temp_rect.x = point.x;
        temp_rect.y = point.y;
        temp_rect.w = selection.selection_start.x-point.x;
        temp_rect.h = selection.selection_start.y-point.y;
    }

    if (temp_rect.x < 0) {temp_rect.x = 0; temp_rect.w = selection.selection_start.x;}
    if ((temp_rect.x + temp_rect.w) > WINDOW_WIDTH) temp_rect.w = WINDOW_WIDTH-temp_rect.x;
    if (temp_rect.y < 0) {temp_rect.y = 0; temp_rect.h = selection.selection_start.y;}
    if ((temp_rect.y + temp_rect.h) > WINDOW_HEIGHT) temp_rect.h = WINDOW_HEIGHT-temp_rect.y;

    selection.selected_area.emplace(std::move(temp_rect));
}

void GameManager::select_obj () {
    std::for_each(objs.begin(), objs.end(),
        [this](Object& o){o.isSelected = SDL_HasIntersection(&selection.selected_area.value(), &o.rect) ? true : false;});

}

void GameManager::select_obj (uint16_t ID) {
    std::for_each(objs.begin(), objs.end(),
                  [ID](Object& o){o.isSelected = (o.getID()==ID ? true : false);});
}

void GameManager::end_select_area() {
    selection.selection_start = {-1,-1};
    selection.selection_end = {-1,-1};
    selection.isSelectionProcess = false;
    selection.selected_area = {};
}

void GameManager::find_place_for_obj(SDL_Point point) {
    for (auto& obj : objs) {
        if (obj.isSelected) {obj.setGoal(point); obj.current_status = Object::NEED_TO_BUILD_PATH;}
    }

    auto res = move_processing.find_where_to_place(objs, base, point);

    for (const auto& [id, mask_point] : res)
        std::for_each(objs.begin(), objs.end(), [id, mask_point](auto& o){if (o.getID() == id) o.setDest(mask_point);});

    std::for_each(objs.begin(), objs.end(), [this](auto& obj){obj.buildPath(objs, base.rect);});
}

void GameManager::process_updates() {

    if (current_game_process >= need_to_reach) gameStatus = GameManager::GAMEOVER_WON;
    if (balance<small_obj.price && objs.empty()) gameStatus = GameManager::GAMEOVER_LOST;
    if (gameStatus != Status::ACTIVE) return;

/////////////////need_to_create_new_diamond
    if(diamonds.size()<diamond_max_size && need_to_create_new_diamond())
        diamonds.emplace_back(get_new_diamond_pos(), std::make_pair(small_diamond.w, small_diamond.h));

/////////////////need_to_create_bomb
    if(bombs.size()<bomb_max_size && need_to_create_new_bomb()) {
        bombs.emplace_back(get_new_bomb_pos(), small_bomb.damage_radius, small_bomb.point_radius);
    }

/////////////////need_to_find_alt_dest
    if (std::any_of(objs.begin(), objs.end(), [](const auto& o){return o.current_status == Object::NEED_TO_FIND_ALT_DEST;})) {
        move_processing.update_map(objs, base);
        std::for_each(objs.begin(), objs.end(), [this](Object& obj){obj.flood_fill(move_processing.getMap());});


        std::vector<SDL_Rect> temp_masks {};
        for (const auto& obj : objs) if (obj.getDestination().has_value() && obj.current_status != Object::NEED_TO_FIND_ALT_DEST)
                    temp_masks.push_back(obj.rect_dest_mask);

        for (auto& obj : objs) {
                if (obj.current_status != Object::NEED_TO_FIND_ALT_DEST) continue;
                if (!temp_masks.empty())
                    obj.add_new_mask(temp_masks);
                if (!obj.find_alt_dest())
                    obj.current_status = Object::DEST_ACHIEVED;
                temp_masks.push_back(obj.rect_dest_mask);
            }
    }

/////////////////need_to_build_path_for_alt_dest
    if (std::any_of(objs.begin(), objs.end(), [](const auto& o){return o.current_status == Object::NEED_TO_BUILD_PATH_FOR_ALT_DEST;}))
            std::for_each(objs.begin(), objs.end(), [this](auto& obj){obj.buildPath(objs, base.rect);});

/////////////////moving
    if (std::any_of(objs.begin(), objs.end(), [](const auto& o){return o.current_status == Object::MOVING;})) {
        for (auto& obj : objs) {
            if (obj.current_status != Object::MOVING) continue;           
            SDL_Point point = move_processing.calc_objs_pos(obj);
            SDL_Rect temp_rect {point.x-obj.rect.w/2, point.y-obj.rect.h/2, obj.rect.w, obj.rect.h};
            bool point_avaiable = true;
            for (const auto& obj_check : objs) {
                if (obj_check.current_status == Object::STATIC && SDL_HasIntersection(&temp_rect, &obj_check.rect)) {
                        obj.setDest({-1,-1});
                        obj.current_status = Object::NEED_TO_BUILD_PATH;                      
                        auto res = move_processing.find_where_to_place(objs, base, obj.getGoalPoint());

                        for (const auto& [id, mask_point] : res)
                            std::for_each(objs.begin(), objs.end(), [id, mask_point](auto& o)
                                          {if (o.getID() == id) o.setDest(mask_point);});
                        obj.buildPath(objs, base.rect);
                        point_avaiable = false;
                        break;
                }
            }
            if (point_avaiable) {
                obj.setPos(point);
                if (point == obj.getPathSegmnets().at(obj.getCurrentSegment()).second) obj.process_next_segment();
            }
            if (obj.getDestination().has_value() && obj.getPos() == obj.getDestination().value())
                obj.current_status = Object::DEST_ACHIEVED;
        }
    }

/////////////////transform_to_static
    if (std::any_of(objs.begin(), objs.end(), [](const auto& o){return o.current_status == Object::DEST_ACHIEVED;})) {
        if (objs.size() == 1 ) {objs[0].current_status = Object::STATIC;} else {
            for (auto& arrived_obj : objs) {
                if (arrived_obj.current_status != Object::DEST_ACHIEVED) continue;

                auto hasIntersection = [&arrived_obj](const auto& o) -> bool {
                    if (arrived_obj.getID() ==  o.getID()) return false;
                    return SDL_HasIntersection (&arrived_obj.rect, &o.rect);
                };
                auto isStuck = [&arrived_obj](const auto& o) -> bool {
                    if ( !(o.current_status == Object::DEST_ACHIEVED || o.current_status == Object::STUCK)
                        || arrived_obj.getID() ==  o.getID()) return false;
                    return SDL_HasIntersection (&arrived_obj.rect, &o.rect);
                };

                if (std::none_of(objs.begin(), objs.end(), hasIntersection)) {
                        arrived_obj.current_status = Object::STATIC;
                        unstuck_thr.erase(arrived_obj.getID());
                        unstuck_results.erase(arrived_obj.getID());
                        arrived_obj.setGoal({-1,-1});
                        arrived_obj.setDest({-1,-1});
                } else if(std::any_of(objs.begin(), objs.end(), isStuck))
                            arrived_obj.current_status = Object::STUCK;
            }
        }
    }

/////////////////process_stuck_obj
    if (std::any_of(objs.begin(), objs.end(), [](const auto& o){return o.current_status == Object::STUCK;})) {

        for (auto& stuck_obj : objs) {
            if (stuck_obj.current_status != Object::STUCK) continue;

            auto hasIntersection = [&stuck_obj](const auto& o) -> bool {
                if (stuck_obj.getID() ==  o.getID()) return false;
                return SDL_HasIntersection (&stuck_obj.rect, &o.rect);
            };

            if (std::none_of(objs.begin(), objs.end(), hasIntersection)) {
                stuck_obj.current_status = Object::STATIC;
                stuck_obj.setGoal({-1,-1});
                stuck_obj.setDest({-1,-1});
            }

            if (!unstuck_thr.contains(stuck_obj.getID()) && stuck_obj.current_status == Object::STUCK) {
                unstuck_results.insert({stuck_obj.getID(), {-1,-1}});
                process_stuck_obj = true;
                std::unique_ptr<std::jthread> unstuck_obj_thr = std::make_unique<std::jthread>([this, &stuck_obj](){
                    Tasker unstuck_processing;
                    Object temp_obj {stuck_obj.getPos().x, stuck_obj.getPos().y, stuck_obj.rect.w, stuck_obj.rect.h, 0, 0};
                    temp_obj.setGoal(stuck_obj.getPos());
                    temp_obj.current_status = Object::NEED_TO_FIND_ALT_DEST;
                    auto stuck_obj_ID = stuck_obj.getID();
                    bool found_available {false};
                    while (process_stuck_obj) {
                        unstuck_processing.update_map(objs, base);
                        temp_obj.flood_fill(unstuck_processing.getMap());
                        std::vector<SDL_Rect> temp_stuck_to_static {};
                        for (const auto& obj : objs) if (obj.current_status == Object::STUCK && obj.getID() != stuck_obj_ID)
                                temp_stuck_to_static.push_back(obj.rect);
                        temp_obj.add_new_mask(temp_stuck_to_static);
                        if (temp_obj.is_possible_to_unstuck()) {
                            found_available = true;
                            break;
                        }
                        SDL_Delay(200);
                    }
                    if (!found_available || !process_stuck_obj) return;

                    temp_obj.current_status = Object::NEED_TO_FIND_ALT_DEST;
                    if (temp_obj.find_alt_dest())
                        unstuck_results.at(stuck_obj_ID) = temp_obj.getDestination().value();
                    });

                unstuck_thr.insert({stuck_obj.getID(), std::move(unstuck_obj_thr)});
            }

            SDL_Point no_destination_p {-1,-1};

            if (unstuck_results.contains(stuck_obj.getID()) && unstuck_results.at(stuck_obj.getID()) != no_destination_p ) {
                process_stuck_obj = false;
                for (const auto& [_, thr] : unstuck_thr) if (thr->joinable()) thr->join();
                stuck_obj.setDest(unstuck_results.at(stuck_obj.getID()));
                stuck_obj.current_status = Object::NEED_TO_BUILD_PATH_FOR_ALT_DEST;
                unstuck_results.clear();
                unstuck_thr.clear();
            }
        }
    }

/////////////////updates_on_Focused_slots
    if (base.lastFocused.has_value()) {
        switch (base.lastFocused.value()) {
        case House::SMALL:
            is_enought_money_to_buy = balance >= small_obj.price ? true : false;
            pre_obj.w = small_obj.w;
            pre_obj.h = small_obj.h;
            break;
        case House::MEDIUM:
            is_enought_money_to_buy = balance >= med_obj.price ? true : false;
            pre_obj.w = med_obj.w;
            pre_obj.h = med_obj.h;
            break;
        case House::LARGE:
            is_enought_money_to_buy = balance >= large_obj.price? true : false;
            pre_obj.w = large_obj.w;
            pre_obj.h = large_obj.h;
            break;
        }
        SDL_Point new_obj_pos = new_obj_processing.find_where_to_place_new_obj(pre_obj, base, objs);
        pre_obj.x = new_obj_pos.x-pre_obj.w/2;
        pre_obj.y = new_obj_pos.y-pre_obj.h/2;
    }

/////////////////update_the_balance && progress
    base.updateBalance(balance);
    base.updateProgress(static_cast<double>(current_game_process)/static_cast<double>(need_to_reach));

/////////////////check_diamond_lifetime&intersection
    bool has_intersection = false;
    auto intersection = [this, &has_intersection](const auto& diamond) -> bool {
        for (const auto& obj : objs) {
            SDL_Point vertex1 {static_cast<int>(diamond.getDiamondVerticies()[0].position.x), static_cast<int>(diamond.getDiamondVerticies()[0].position.y)};
            SDL_Point vertex2 {static_cast<int>(diamond.getDiamondVerticies()[1].position.x), static_cast<int>(diamond.getDiamondVerticies()[1].position.y)};
            SDL_Point vertex3 {static_cast<int>(diamond.getDiamondVerticies()[2].position.x), static_cast<int>(diamond.getDiamondVerticies()[2].position.y)};
            SDL_Point vertex4 {static_cast<int>(diamond.getDiamondVerticies()[3].position.x), static_cast<int>(diamond.getDiamondVerticies()[3].position.y)};

            if (SDL_IntersectRectAndLine(&obj.rect, &vertex1.x, &vertex1.y, &vertex2.x, &vertex2.y) ||
                SDL_IntersectRectAndLine(&obj.rect, &vertex2.x, &vertex2.y, &vertex3.x, &vertex3.y) ||
                SDL_IntersectRectAndLine(&obj.rect, &vertex3.x, &vertex3.y, &vertex4.x, &vertex4.y) ||
                SDL_IntersectRectAndLine(&obj.rect, &vertex4.x, &vertex4.y, &vertex1.x, &vertex1.y)) {
                has_intersection = true;
                return true;
            }
        }
        return false;
    };
    std::erase_if(diamonds, [this, &intersection](const auto& d){return (d.get_time()>diamond_limit_time) || intersection(d);});
    if (has_intersection) balance += small_diamond.value;

/////////////////bombs && damages
    for (auto& bomb : bombs) {
        if (bomb.current_status == TimerBomb::PRE_EXPLOSION && bomb.time_to_blowUp()) {
            bomb.current_status = TimerBomb::EXPLOSION_PROCESS;
            bomb.set_start_explosion_time(SDL_GetTicks());
        } else if (bomb.current_status == TimerBomb::EXPLOSION_PROCESS) {
            bomb.update_current_explosion_radius();

            for (auto& obj : objs) {
                double dx = bomb.get_position().x - obj.getPos().x;
                double dy = bomb.get_position().y - obj.getPos().y;
                double distance = sqrt(dx * dx + dy * dy);
                if (distance < bomb.get_current_explosion_radius()) {
                    double damage_ratio;
                    double distance_ratio {static_cast<double>(bomb.get_current_explosion_radius())/static_cast<double>(bomb.get_damage_radius())};
                    if (distance_ratio < 0.5) damage_ratio = 1; else damage_ratio = ((1-(distance_ratio-0.5)*2)*0.9)+0.1;
                    if (!bomb.damaged_obj_ids.count(obj.getID())) {
                        bomb.damaged_obj_ids.insert(obj.getID());
                            double health_dif = static_cast<double>(obj.get_current_health()) - static_cast<double>(bomb.get_damage_value())*damage_ratio;
                        if (health_dif < 0) {
                            obj.set_current_health(0);
                            erase_if(unstuck_results, [&obj](const auto& el){return el.first == obj.getID();});
                        }
                        else obj.set_current_health(static_cast<uint16_t>(health_dif));
                    }
                }
            }
            if (bomb.get_current_explosion_radius()>bomb.get_damage_radius())
                bomb.current_status = TimerBomb::EXPIRED;
        }
    }
    std::erase_if(bombs, [](const auto& b){return b.current_status == TimerBomb::EXPIRED;});

/////////////////process_0health_obj
    std::erase_if(objs, [](const auto& o){return o.get_current_health() == 0;});

/////////////////update_game_process
    current_game_process = 0;
    for (const auto& obj : objs) current_game_process += obj.get_max_health();

}

void GameManager::setRenderer (SDL_Renderer* renderer) {
    this->renderer = renderer;
}

void GameManager::init_font (const std::string& path) {
    std::ifstream fs(path.c_str(), std::ios::in | std::ios::binary);
    if (!fs.is_open()) {
        std::cout << "readFileRaw: " << path << " -- " << "WARNING: Could not open file." << std::endl;
        std::terminate();
    }
    fs.seekg (0, std::ios::end);
    const size_t LEN = fs.tellg();
    fs.seekg (0, std::ios::beg);
    mem.alloc(LEN);
    fs.read(mem.data, LEN);
    fs.close();
    base.processFont(mem, renderer, {small_obj.price, med_obj.price, large_obj.price});
}

bool GameManager::isFocusedOnBaseSlots(const SDL_Point point) const {
    return base.isPointInsideSlots(point);
}

void GameManager::process_popup() {
    //additional pop-up processing
}

bool GameManager::need_to_create_new_diamond () {
    if ((SDL_GetTicks()-time_since_last_diamond_created) > diamond_appear_interval) {
        time_since_last_diamond_created = SDL_GetTicks();

        std::mt19937 generator( os_seed() );
        std::uniform_int_distribution< unsigned int > distribute(0,100);
        return (diamond_appear_chance+objs.size()) > distribute(generator) ? true : false;
    } else return false;
}

SDL_Point GameManager::get_new_diamond_pos () {
    std::mt19937 generator( os_seed() );
    bool found_place {false};
    SDL_Point point;
    do {
      std::uniform_int_distribution< int > distribute(0, WINDOW_WIDTH*WINDOW_HEIGHT-1);
      int index = distribute(generator);
      point = {index%WINDOW_WIDTH, index/WINDOW_WIDTH};
      SDL_Rect temp_diamond_rect {point.x, point.y, small_diamond.w, small_diamond.h};

      if(temp_diamond_rect.x<0 || temp_diamond_rect.y<0 ||
          (temp_diamond_rect.x+temp_diamond_rect.w) > WINDOW_WIDTH ||
          (temp_diamond_rect.y+temp_diamond_rect.h) > WINDOW_HEIGHT)
            continue;      

      if (std::none_of(objs.begin(), objs.end(),
                       [&temp_diamond_rect](const auto & obj){return SDL_HasIntersection(&obj.rect, &temp_diamond_rect);})
          && std::none_of(diamonds.begin(), diamonds.end(),
                       [&temp_diamond_rect](const auto & d){return SDL_HasIntersection(&d.rect, &temp_diamond_rect);})
          && !SDL_HasIntersection(&temp_diamond_rect, &base.rect))
            found_place = true;

    } while (!found_place);

    return point;
}

bool GameManager::need_to_create_new_bomb () {
    if ((SDL_GetTicks()-time_since_last_bomb_created) > bomb_appear_interval) {
      time_since_last_bomb_created = SDL_GetTicks();

      std::mt19937 generator( os_seed() );
      std::uniform_int_distribution< unsigned int > distribute(0,100);
      return (bomb_appear_chance+objs.size()/4) > distribute(generator) ? true : false;
    } else return false;
}

SDL_Point GameManager::get_new_bomb_pos () {
    std::mt19937 generator( os_seed() );
    bool found_place {false};
    SDL_Point point;
    do {
      std::uniform_int_distribution< int > distribute(0, WINDOW_WIDTH*WINDOW_HEIGHT-1);
      int index = distribute(generator);
      point = {index%WINDOW_WIDTH, index/WINDOW_WIDTH};
      SDL_Rect temp_bomb_point_rect {point.x, point.y, small_bomb.point_radius, small_bomb.point_radius};

      if(temp_bomb_point_rect.x<0 || temp_bomb_point_rect.y<0 ||
          (temp_bomb_point_rect.x+temp_bomb_point_rect.w) > WINDOW_WIDTH ||
          (temp_bomb_point_rect.y+temp_bomb_point_rect.h) > WINDOW_HEIGHT)
            continue;

      if (std::none_of(objs.begin(), objs.end(),
                       [&temp_bomb_point_rect](const auto & obj){return SDL_HasIntersection(&obj.rect, &temp_bomb_point_rect);})
          && std::none_of(diamonds.begin(), diamonds.end(),
                          [&temp_bomb_point_rect](const auto & d){return SDL_HasIntersection(&d.rect, &temp_bomb_point_rect);})
          && !SDL_HasIntersection(&temp_bomb_point_rect, &base.rect))
            found_place = true;

    } while (!found_place);

    return point;
}

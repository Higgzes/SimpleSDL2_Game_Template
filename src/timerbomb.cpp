#include "timerbomb.h"

TimerBomb::TimerBomb(SDL_Point position, uint16_t damage_radius, uint16_t point_radius)
    : damage_radius(damage_radius),
    point_radius(point_radius),
    time_created(SDL_GetTicks()),
    position(position),
    current_status(PRE_EXPLOSION)
{}

bool TimerBomb::time_to_blowUp() const {
    return (SDL_GetTicks()-time_created) > pre_explosion_time ? true : false;
}

void TimerBomb::update_current_explosion_radius () {
    current_radius = (SDL_GetTicks()-start_explosion_time)*damage_radius*explosion_speed/100000;
}

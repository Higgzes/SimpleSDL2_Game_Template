#include "object.h"

Object::Object(int x, int y, int w, int h, uint16_t speed, uint16_t health, SDL_Color color)
        : current_status(status::STATIC), speed (speed) {
    id = BaseObject::current_id++;
    this->color = color;
    rect.w = w;
    rect.h = h;
    rect.x = x-w/2;
    rect.y = y-h/2;
    current_health = health;
    max_health = health;
    path.distances.reserve(WINDOW_WIDTH*WINDOW_HEIGHT); 
    std::fill_n(&area_available_c[0][0], WINDOW_HEIGHT*WINDOW_WIDTH, 1);
}

////////////PRIVATE METHODS////////////

void Object::fill_area_alg (int16_t x, int16_t y) {
//C-Style algorithm taken from https://www.codeproject.com/Articles/5312368/Five-Fast-Flood-Fills
    std::copy (std::begin(border_mask_c), std::end(border_mask_c), &area_available_c[0][0]);
    for (int i=0;i<WINDOW_WIDTH; i++) {
        area_available_c[rect.h/2-1][i] = 0;
        area_available_c[WINDOW_HEIGHT-rect.h/2][i] = 0;
    }
    for (int i=0; i<WINDOW_HEIGHT; i++) {
        area_available_c[i][rect.w/2-1] = 0;
        area_available_c[i][WINDOW_WIDTH-rect.w/2] = 0;
    }

    int stack[MAX_DIM];
    int stackPointer;
    int tArr0[MAX_DIM], t1Arr0[MAX_DIM];
    int  x1, x2, x3, x4, y1, y2, y3, y4, z, p, j;
    char av_point;
    stackPointer = 0;
    int* tArr = tArr0;
    int* t1Arr = t1Arr0;
    int* between;
    int tPt = 0, pPt = 0, t1Pt = 0;
    av_point = area_available_c[y][x];

    path.distances.clear();

    stack[stackPointer] = x;
    stackPointer++;
    stack[stackPointer] = y;
    stackPointer++;
    stack[stackPointer] = 1;
    stackPointer++;
    for (j = 0; j < stackPointer;) {
        x1 = stack[j];
        j++;
        y = stack[j];
        j++;
        z = stack[j];
        j++;
        x2 = x1;
        while (area_available_c[y][x2] == av_point) {
            area_available_c[y][x2] = 2;

            float dist = sqrtf(std::pow(path.goal_p.x - x2, 2) + std::pow(path.goal_p.y - y, 2));
            SDL_Point p {x2,y};
            path.distances.emplace_back(std::make_pair(dist, p));

            x2++;
            if (x2 == MAX_DIM) break;
        }
        x1--;

        while (area_available_c[y][x1] == av_point) {
            area_available_c[y][x1] = 2;

            float dist = sqrtf(std::pow(path.goal_p.x - x1, 2) + std::pow(path.goal_p.y - y, 2));
            SDL_Point p {x1,y};
            path.distances.emplace_back(std::make_pair(dist, p));

            x1--;
            if (x1 == -1) break;
        }
        x1++;
        if (x1 > x2) continue;
        tPt = 0;
        tArr[tPt] = x1;
        tPt++;
        tArr[tPt] = x2;
        tPt++;
        if (z == 1) {
            y4 = y - 1;
            if (y4 > -1) {
                x = x1;
                while (x < x2) {
                    while (area_available_c[y4][x] != av_point) {
                        x++;
                        if (x == x2) break;
                        }
                    if (x < x2) {
                        stack[stackPointer] = x;
                        stackPointer++;
                        stack[stackPointer] = y4;
                        stackPointer++;
                        stack[stackPointer] = -1;
                        stackPointer++;
                        while (area_available_c[y4][x] == av_point) {
                            x++;
                            if (x == x2) break;
                        }
                    }
                }
            }

            for (y1 = y + 1; y1 < MAX_DIM; y1++) {
                pPt = 0;
                t1Pt = 0;
                y4 = y1 - 1;
                while (pPt < tPt) {
                    x3 = tArr[pPt];
                    pPt++;
                    x4 = tArr[pPt];
                    pPt++;
                    x2 = x3 + 1;
                    x1 = x3;
                    while (area_available_c[y1][x1] == av_point) {
                        area_available_c[y1][x1] = 2;

                        float dist = sqrtf(std::pow(path.goal_p.x - x1, 2) + std::pow(path.goal_p.y - y1, 2));
                        SDL_Point p {x1,y1};
                        path.distances.emplace_back(std::make_pair(dist, p));

                        x1--;
                        if (x1 == -1) break;
                    }
                    x1++;
                    x = x1;
                    while (x < x3) {
                        while (area_available_c[y4][x] != av_point) {
                            x++;
                            if (x == x3) break;
                        }
                        if (x < x3)
                        {
                            stack[stackPointer] = x;
                            stackPointer++;
                            stack[stackPointer] = y4;
                            stackPointer++;
                            stack[stackPointer] = -1;
                            stackPointer++;
                            while (area_available_c[y4][x] == av_point) {
                                x++;
                                if (x == x3) break;
                            }
                        }
                    }
                    while (x1 < x4) {
                        while (area_available_c[y1][x2] == av_point) {
                            area_available_c[y1][x2] = 2;

                            float dist = sqrtf(std::pow(path.goal_p.x - x2, 2) + std::pow(path.goal_p.y - y1, 2));
                            SDL_Point p {x2,y1};
                            path.distances.emplace_back(std::make_pair(dist, p));

                            x2++;
                            if (x2 == MAX_DIM) break;
                        }
                        if (x1 < x2) {
                            t1Arr[t1Pt] = x1;
                            t1Pt++;
                            t1Arr[t1Pt] = x2;
                            t1Pt++;
                        }
                        while (x2 < x4 && area_available_c[y1][x2] != av_point)
                            x2++;

                        x1 = x2;
                    }
                    x = x4;
                    while (x < x2) {
                        while (area_available_c[y4][x] != av_point) {
                            x++;
                            if (x == x2) break;
                        }
                        if (x < x2) {
                            stack[stackPointer] = x;
                            stackPointer++;
                            stack[stackPointer] = y4;
                            stackPointer++;
                            stack[stackPointer] = -1;
                            stackPointer++;
                            while (area_available_c[y4][x] == av_point) {
                                x++;
                                if (x == x2) break;
                            }
                        }
                    }
                }
                if (t1Pt == 0) break;
                between = tArr;
                tArr = t1Arr;
                t1Arr = between;
                tPt = t1Pt;
            }
        }
        else {
            y4 = y + 1;
            if (y4 < MAX_DIM) {
                x = x1;
                while (x < x2) {
                    while (area_available_c[y4][x] != av_point) {
                        x++;
                        if (x == x2) break;
                    }
                    if (x < x2) {
                        stack[stackPointer] = x;
                        stackPointer++;
                        stack[stackPointer] = y4;
                        stackPointer++;
                        stack[stackPointer] = 1;
                        stackPointer++;
                        while (area_available_c[y4][x] == av_point) {
                            x++;
                            if (x == x2) break;
                        }
                    }
                }
            }
           for (y1 = y - 1; y1 > -1; y1--) {
                pPt = 0;
                t1Pt = 0;
                y4 = y1 + 1;
                while (pPt < tPt) {
                    x3 = tArr[pPt];
                    pPt++;
                    x4 = tArr[pPt];
                    pPt++;
                    x2 = x3 + 1;
                    x1 = x3;
                    while (area_available_c[y1][x1] == av_point) {
                        area_available_c[y1][x1] = 2;

                        float dist = sqrtf(std::pow(path.goal_p.x - x1, 2) + std::pow(path.goal_p.y - y1, 2));
                        SDL_Point p {x1,y1};
                        path.distances.emplace_back(std::make_pair(dist, p));

                        x1--;
                        if (x1 == -1) break;
                    }
                    x1++;
                    x = x1;
                    while (x < x3) {
                        while (area_available_c[y4][x] != av_point) {
                            x++;
                            if (x == x3) break;
                        }
                        if (x < x3) {
                            stack[stackPointer] = x;
                            stackPointer++;
                            stack[stackPointer] = y4;
                            stackPointer++;
                            stack[stackPointer] = 1;
                            stackPointer++;
                            while (area_available_c[y4][x] == av_point) {
                                x++;
                                if (x == x3) break;
                            }
                        }
                    }
                    while (x1 < x4) {
                       while (area_available_c[y1][x2] == av_point) {
                            area_available_c[y1][x2] = 2;

                            float dist = sqrtf(std::pow(path.goal_p.x - x2, 2) + std::pow(path.goal_p.y - y1, 2));
                            SDL_Point p {x2,y1};
                            path.distances.emplace_back(std::make_pair(dist, p));

                            x2++;
                            if (x2 == MAX_DIM) break;
                        }
                        if (x1 < x2) {
                            t1Arr[t1Pt] = x1;
                            t1Pt++;
                            t1Arr[t1Pt] = x2;
                            t1Pt++;
                        }
                        while (x2 < x4 && area_available_c[y1][x2] != av_point)
                            x2++;
                        x1 = x2;
                    }
                    x = x4;
                    while (x < x2) {
                        while (area_available_c[y4][x] != av_point) {
                            x++;
                            if (x == x2) break;
                        }
                        if (x < x2) {
                            stack[stackPointer] = x;
                            stackPointer++;
                            stack[stackPointer] = y4;
                            stackPointer++;
                            stack[stackPointer] = 1;
                            stackPointer++;
                            while (area_available_c[y4][x]  == av_point) {
                                x++;
                                if (x == x2) break;
                            }
                        }
                    }
                }
                if (t1Pt == 0) break;
                between = tArr;
                tArr = t1Arr;
                t1Arr = between;
                tPt = t1Pt;
            }
        }
    }
}

bool Object::createNavmesh (const std::vector<Object>& objs, const SDL_Rect& base_rect) {
    navmesh.reset();
    triangle::triangleio inputData;
    triangle::triangle_initialize_triangleio(&inputData);

    int static_obj_and_masks{0};
    int v_id{0};
    for (const auto& obj : objs)
        if (obj.current_status == STATIC) static_obj_and_masks++;
    int total_vertex = (static_obj_and_masks+3) * 4;
    inputData.numberofpoints = total_vertex;
    inputData.numberofsegments = total_vertex;
    inputData.pointlist = new double[2*total_vertex];
    inputData.segmentlist = new int[2*total_vertex];
    inputData.segmentmarkerlist = new int[total_vertex];

    for (int i = 0; i < total_vertex; i++) inputData.segmentmarkerlist[i] = 3;

    int w = rect.w/2;
    int h = rect.h/2;

    inputData.pointlist[0] = w-0.2;
    inputData.pointlist[1] = h-0.2;
    inputData.pointlist[2] = static_cast<double>(WINDOW_WIDTH)+0.2-w;
    inputData.pointlist[3] = h-0.2;
    inputData.pointlist[4] = static_cast<double>(WINDOW_WIDTH)+0.2-w;
    inputData.pointlist[5] = static_cast<double>(WINDOW_HEIGHT)+0.2-h;
    inputData.pointlist[6] = w-0.2;
    inputData.pointlist[7] = static_cast<double>(WINDOW_HEIGHT)+0.2-h;
    inputData.pointlist[8] = 0;
    inputData.pointlist[9] = 0;
    inputData.pointlist[10] = static_cast<double>(WINDOW_WIDTH);
    inputData.pointlist[11] = 0;
    inputData.pointlist[12] = static_cast<double>(WINDOW_WIDTH);
    inputData.pointlist[13] = static_cast<double>(WINDOW_HEIGHT);
    inputData.pointlist[14] = 0;
    inputData.pointlist[15] = static_cast<double>(WINDOW_HEIGHT);
    inputData.pointlist[16] = (base_rect.x-w) <= 0     ? 0.2 : base_rect.x-w+0.2;
    inputData.pointlist[17] = (base_rect.y-h) <= 0     ? 0.2 : base_rect.y-h+0.2;
    inputData.pointlist[18] = (base_rect.x+base_rect.w+w) >= WINDOW_WIDTH ? WINDOW_WIDTH-0.2 : base_rect.x+base_rect.w+w-0.2;
    inputData.pointlist[19] = (base_rect.y-h) <= 0     ? 0.2 : base_rect.y-h+0.2;
    inputData.pointlist[20] = (base_rect.x+base_rect.w+w) >= WINDOW_WIDTH ? WINDOW_WIDTH-0.2 : base_rect.x+base_rect.w+w-0.2;
    inputData.pointlist[21] = (base_rect.y+base_rect.h+h) >= WINDOW_HEIGHT ? WINDOW_HEIGHT-0.2 : base_rect.y+base_rect.h+h-0.2;
    inputData.pointlist[22] = (base_rect.x-w) <= 0     ? 0.2 : base_rect.x-w+0.2;
    inputData.pointlist[23] = (base_rect.y+base_rect.h+h) >= WINDOW_HEIGHT ? WINDOW_HEIGHT-0.2 : base_rect.y+base_rect.h+h-0.2;


    inputData.segmentlist[0] = 0;
    inputData.segmentlist[1] = 1;
    inputData.segmentlist[2] = 1;
    inputData.segmentlist[3] = 2;
    inputData.segmentlist[4] = 2;
    inputData.segmentlist[5] = 3;
    inputData.segmentlist[6] = 3;
    inputData.segmentlist[7] = 0;
    inputData.segmentlist[8] = 4;
    inputData.segmentlist[9] = 5;
    inputData.segmentlist[10] = 5;
    inputData.segmentlist[11] = 6;
    inputData.segmentlist[12] = 6;
    inputData.segmentlist[13] = 7;
    inputData.segmentlist[14] = 7;
    inputData.segmentlist[15] = 4;
    inputData.segmentlist[16] = 8;
    inputData.segmentlist[17] = 9;
    inputData.segmentlist[18] = 9;
    inputData.segmentlist[19] = 10;
    inputData.segmentlist[20] = 10;
    inputData.segmentlist[21] = 11;
    inputData.segmentlist[22] = 11;
    inputData.segmentlist[23] = 8;

    int list_count = 23;

    for (const auto& obj : objs) {
        if (obj.current_status != STATIC) continue;

        int x = obj.rect.x;
        int y = obj.rect.y;

        inputData.pointlist[++list_count]       = (x-w) <= 0     ? 0.2 : x-w+0.2;
        inputData.pointlist[++list_count]       = (y-h) <= 0     ? 0.2 : y-h+0.2;
        inputData.pointlist[++list_count]       = (x+obj.rect.w+w) >= WINDOW_WIDTH ? WINDOW_WIDTH-0.2 : x+obj.rect.w+w-0.2;
        inputData.pointlist[++list_count]       = (y-h) <= 0     ? 0.2 : y-h+0.2;
        inputData.segmentlist[(list_count-3)]   = list_count/2-1;
        inputData.segmentlist[(list_count-2)]   = list_count/2;
        inputData.pointlist[++list_count]       = (x+obj.rect.w+w) >= WINDOW_WIDTH ? WINDOW_WIDTH-0.2 : x+obj.rect.w+w-0.2;
        inputData.pointlist[++list_count]       = (y+obj.rect.h+h) >= WINDOW_HEIGHT ? WINDOW_HEIGHT-0.2 : y+obj.rect.h+h-0.2;
        inputData.segmentlist[(list_count-3)]   = list_count/2-1;
        inputData.segmentlist[(list_count-2)]   = list_count/2;
        inputData.pointlist[++list_count]       = (x-w) <= 0    ? 0.2 : x-w+0.2;
        inputData.pointlist[++list_count]       = (y+obj.rect.h+h) >= WINDOW_HEIGHT ? WINDOW_HEIGHT-0.2 : y+obj.rect.h+h-0.2;
        inputData.segmentlist[(list_count-3)]   = list_count/2-1;
        inputData.segmentlist[(list_count-2)]   = list_count/2;
        inputData.segmentlist[(list_count-1)]   = list_count/2;
        inputData.segmentlist[(list_count)]     = list_count/2-3;

    }

    triangle::context *ctx;
    ctx = triangle::triangle_context_create();
    pathfinder::BehaviorBuilder behaviorBuilder_;
    *(ctx->b) = behaviorBuilder_.getBehavior();

    int mesh_create = triangle::triangle_mesh_create(ctx, &inputData);

    if(mesh_create != 0) {
        std::cout<<"Error creating navmesh "; // #TODO: ######Need to fix it somehow ???#####
        // TRI_SEG_INTERSECT (-6) library error: segmentintersection()
        // Topological inconsistency after splitting a segment.
        writePoly(inputData, "error_mesh.poly"); //for debug
        std::cout<<"Error: " << mesh_create <<std::endl;

        delete[] inputData.pointlist;
        delete[] inputData.segmentlist;
        delete[] inputData.segmentmarkerlist;
        triangle_context_destroy(ctx);
        return false;
    };

    triangle::triangleio triangleData, triangleVoronoiData;
    triangle::triangle_initialize_triangleio(&triangleData);
    triangle::triangle_initialize_triangleio(&triangleVoronoiData);

    triangle::triangle_mesh_copy(ctx, &triangleData, 1, 1, &triangleVoronoiData);
    triangle_context_destroy(ctx);

    navmesh = std::shared_ptr<pathfinder::navmesh::NavmeshInterface>
        (new pathfinder::navmesh::TriangleLibNavmesh(triangleData, triangleVoronoiData));

    triangle_free_triangleio(&triangleData);
    triangle_free_triangleio(&triangleVoronoiData);

    delete[] inputData.pointlist;
    delete[] inputData.segmentlist;
    delete[] inputData.segmentmarkerlist;
    return true;
}

void Object::createMask(const bool* map) {
    std::fill(std::begin(border_mask_c), std::end(border_mask_c), true);

    for (int i = 0; i < WINDOW_WIDTH*WINDOW_HEIGHT; i++) {
        if (map[i]) continue;
        if (abs((i%WINDOW_WIDTH)-((i+1)%WINDOW_WIDTH)) == 1 &&
            ((i+WINDOW_WIDTH) < WINDOW_WIDTH*WINDOW_HEIGHT) &&
            !map[i+1] && !map[WINDOW_WIDTH+i] && map[WINDOW_WIDTH+i+1]) {

            for (int n = 0; n <= rect.w/2; n++) {
                if ( ((i-rect.w/2+n)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i-(WINDOW_WIDTH*rect.h/2) > 0) )
                {   int index = (i-rect.w/2+n+1)-(WINDOW_WIDTH*(rect.h/2-1));
                border_mask_c[index] = false;
                }
            }
            for (int m = 0; m <= rect.h/2-1; m++) {
                if ( ((i-rect.w/2)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i-(WINDOW_WIDTH*(rect.h/2-m)) > 0) )
                {   int index = (i-rect.w/2+1)-(WINDOW_WIDTH*(rect.h/2-m-1));
                border_mask_c[index] = false;
                }
            }
        } else if (abs((i%WINDOW_WIDTH)-((i-1)%WINDOW_WIDTH)) == 1 &&
                   ((i+WINDOW_WIDTH) < WINDOW_WIDTH*WINDOW_HEIGHT) &&
                   !map[i-1] && !map[WINDOW_WIDTH+i] && map[WINDOW_WIDTH+i-1]) {

            for (int n = 0; n <= rect.w/2; n++) {
                if ( ((i+n)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i-(WINDOW_WIDTH*rect.h/2) > 0) )
                {
                int index = (i+n)-(WINDOW_WIDTH*(rect.h/2-1));
                border_mask_c[index] = false;
                }
            }
            for (int m = 0; m <= rect.h/2-1; m++) {
                if ( ((i+rect.w/2)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i-(WINDOW_WIDTH*(rect.h/2-m)) > 0) )
                {
                int index = (i+rect.w/2)-(WINDOW_WIDTH*(rect.h/2-m-1));
                border_mask_c[index] = false;
                }
            }
        } else if (abs((i%WINDOW_WIDTH)-((i-1)%WINDOW_WIDTH)) == 1 &&
                   (i - WINDOW_WIDTH >= 0) &&
                   !map[i-1] && !map[i-WINDOW_WIDTH] && map[i-WINDOW_WIDTH-1]) {

            for (int n = 0; n <= rect.w/2-1; n++) {
                if ( ((i+n)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i+(WINDOW_WIDTH*rect.h/2) < WINDOW_WIDTH*WINDOW_HEIGHT) )
                {
                int index = (i+n)+(WINDOW_WIDTH*rect.h/2);
                border_mask_c[index] = false;
                }
            }
            for (int m = 0; m <= rect.h/2; m++) {
                if ( ((i+rect.w/2)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i+(WINDOW_WIDTH*(rect.h/2-m)) > 0) )
                {
                int index = (i+rect.w/2)+(WINDOW_WIDTH*(rect.h/2-m));
                border_mask_c[index] = false;
                }
            }
        } else if (abs((i%WINDOW_WIDTH)-((i+1)%WINDOW_WIDTH)) == 1 &&
                   (i - WINDOW_WIDTH >= 0) &&
                   !map[i+1] && !map[i-WINDOW_WIDTH] && map[i-WINDOW_WIDTH+1]) {

            for (int n = 0; n <= rect.w/2; n++) {
                if ( ((i-rect.w/2+n)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i+(WINDOW_WIDTH*rect.h/2) < WINDOW_WIDTH*WINDOW_HEIGHT) )
                {
                int index = (i-rect.w/2+n+1)+(WINDOW_WIDTH*rect.h/2);
                border_mask_c[index] = false;
                }
            }
            for (int m = 0; m <= rect.h/2; m++) {
                if ( ((i-rect.w/2)/WINDOW_WIDTH == i/WINDOW_WIDTH) && (i+(WINDOW_WIDTH*(rect.h/2-m)) < WINDOW_WIDTH*WINDOW_HEIGHT) )
                {
                int index = (i-rect.w/2+1)+(WINDOW_WIDTH*(rect.h/2-m));
                border_mask_c[index] = false;
                }
            }
        } else if ((WINDOW_WIDTH+i < WINDOW_WIDTH*WINDOW_HEIGHT) && (i-WINDOW_WIDTH >= 0) &&
                   (i-(rect.h/2*WINDOW_WIDTH) >= 0) && (i+(rect.h/2*WINDOW_WIDTH) < WINDOW_WIDTH*WINDOW_HEIGHT) &&
                   map[WINDOW_WIDTH+i] && map[i-WINDOW_WIDTH]) {
            int indexHorizUp = i-((rect.h/2-1)*WINDOW_WIDTH);
            border_mask_c[indexHorizUp] = false;
            int indexHorizDown = i+(rect.h/2*WINDOW_WIDTH);
            border_mask_c[indexHorizDown] = false;
        } else if (abs((i%WINDOW_WIDTH)-((i+1)%WINDOW_WIDTH)) == 1 && abs((i%WINDOW_WIDTH)-((i-1)%WINDOW_WIDTH)) == 1 &&
                   (i+rect.w/2)/WINDOW_WIDTH == (i-rect.w/2)/WINDOW_WIDTH &&
                   map[i+1] && map[i-1]) {
            int indexVertLeft = i-rect.w/2+1;
            border_mask_c[indexVertLeft] = false;
            int indexVertRight = i+rect.w/2;
            border_mask_c[indexVertRight] = false;
        }
    }
}

void Object::writePoly(const auto& inputData, std::string filename) {
    std::string res;
    res += std::to_string(inputData.numberofpoints);
    res += " 2 0 0\n";
    for (int i=0; i<inputData.numberofpoints; i++) {
        res += std::to_string(i);
        res += " ";
        res += std::to_string(inputData.pointlist[2*i]);
        res += " ";
        res += std::to_string(inputData.pointlist[2*i + 1]);
        res += '\n';
    }
    res += std::to_string(inputData.numberofpoints);
    res += " 1\n";
    for (int i=0; i<inputData.numberofsegments; i++) {
        res += std::to_string(i);
        res += " ";
        res += std::to_string(inputData.segmentlist[2*i]);
        res += " ";
        res += std::to_string(inputData.segmentlist[2*i + 1]);
        res += " ";
        res += std::to_string(inputData.segmentmarkerlist[i]);
        res += '\n';
    }
    res += "0";
    std::ofstream out(filename);
    out << res;
    out.close();
}

////////////PUBLIC METHODS////////////

bool Object::operator == (const Object& obj) const {
    return SDL_RectEquals(&this->rect, &obj.rect);
}

SDL_Color Object::getColor() const {
    double health_ration = static_cast<double>(current_health)/static_cast<double>(max_health);

    Uint8 o = 250-health_ration*250;
    return {255, o, o, 255};
}

std::optional<SDL_Point> Object::getDestination () const {
    if (path.destination_p == SDL_Point{-1, -1}) return {};
    return path.destination_p;
}

void Object::setDest(const SDL_Point point) {
    path.destination_p = point;
    if (getDestination().has_value())
        rect_dest_mask = {point.x - rect.w/2, point.y - rect.h/2, rect.w, rect.h};
}

void Object::setGoal(const SDL_Point point) {
    path.goal_p = point;
    path.start_p = getPos();
}

void Object::setPos(const SDL_Point point) {
    rect.x = point.x-rect.w/2; rect.y = point.y-rect.h/2;
}

void Object::buildPath (const std::vector<Object>& objs, const SDL_Rect& base_rect) {
    if (!(current_status == NEED_TO_BUILD_PATH || current_status == NEED_TO_BUILD_PATH_FOR_ALT_DEST)) return;

    if (!createNavmesh(objs, base_rect)) {
        current_status = DEST_ACHIEVED;
        return;
    }

    mu.lock();
    pathfinder::Pathfinder pathfinder(reinterpret_cast<pathfinder::navmesh::AStarNavmeshInterface&>(*navmesh.get()), 0);
    mu.unlock();
    std::optional<pathfinder::Vector> startPoint_{std::in_place, rect.x+rect.w/2, rect.y+rect.h/2};
    std::optional<pathfinder::Vector> goalPoint_{std::in_place, path.destination_p.x, path.destination_p.y};

    auto pathfindingResult_ = pathfinder.findShortestPath(*startPoint_, *goalPoint_);

    if (pathfindingResult_.shortestPath.empty()) {
        current_status = NEED_TO_FIND_ALT_DEST;
        setDest({-1,-1});
        return;
    }
    path.segments.clear();
    hasPath = true;
    for  (auto& segment : pathfindingResult_.shortestPath) {
        int seg_x_start = round(static_cast<pathfinder::StraightPathSegment*>(segment.get())->startPoint.x());
        int seg_y_start = round(static_cast<pathfinder::StraightPathSegment*>(segment.get())->startPoint.y());
        int seg_x_end = round(static_cast<pathfinder::StraightPathSegment*>(segment.get())->endPoint.x());
        int seg_y_end = round(static_cast<pathfinder::StraightPathSegment*>(segment.get())->endPoint.y());
        path.segments.emplace_back(std::make_pair<SDL_Point, SDL_Point>({seg_x_start, seg_y_start}, {seg_x_end, seg_y_end}));
    }
    current_status = MOVING;
    path.prevTicks = SDL_GetPerformanceCounter();
    path.current_segment = 0;
}

void Object::flood_fill  (const bool* map_mask) {
    if (current_status != NEED_TO_FIND_ALT_DEST) return;
    createMask(map_mask);
    fill_area_alg(getPos().x, getPos().y);
}

bool Object::find_alt_dest() {
    if (current_status != NEED_TO_FIND_ALT_DEST) return false;

    std::sort(std::execution::par_unseq, path.distances.begin(), path.distances.end(), [](const auto& lhs, const auto& rhs){return lhs.first < rhs.first;});

    for (int i = 0; i<path.distances.size(); i++) {
        SDL_Point p = path.distances[i].second;
        if (area_available_c[p.y][p.x] == 2) {
            setDest({path.distances[i].second.x, path.distances[i].second.y});
            current_status = NEED_TO_BUILD_PATH_FOR_ALT_DEST;
            if (getDestination().value() == getPos()) current_status = DEST_ACHIEVED;
            return true;
        }
    }
    return false;
}

void Object::add_new_mask (const std::vector<SDL_Rect>& masks) {
    for (const auto& mask_rect : masks) {
        for (int n = 0; n < mask_rect.w+rect.w; n++) {
            for (int m = 0; m < mask_rect.h+rect.h; m++) {
                if ((mask_rect.y+m-rect.h/2) >= WINDOW_HEIGHT ||
                    (mask_rect.y+m-rect.h/2) < 0 ||
                    (mask_rect.x+n-rect.w/2) >= WINDOW_WIDTH ||
                    (mask_rect.x+n-rect.w/2) < 0 )
                        continue;
                int x = mask_rect.x+n-rect.w/2; int y = mask_rect.y+m-rect.h/2;
                area_available_c[y][x] = 1;
            }
        }
    }
}

void Object::process_next_segment() {
    path.current_segment++;
    path.prevTicks = SDL_GetPerformanceCounter();
    if (path.current_segment == path.segments.size()) {
        path.current_segment = 0;
        hasPath = false;
    }
}

bool Object::is_possible_to_unstuck () const {
    for (int x = 0; x<WINDOW_WIDTH; x++) {
        for (int y = 0; y<WINDOW_HEIGHT; y++)
            if (area_available_c[y][x] == 2) return true;
    }
    return false;
}

#include "house.h"

House::House(int x, int y, int w, int h, SDL_Color color)
{
    id = BaseObject::current_id++;
    this->color = color;
    rect.w = w;
    rect.h = h;
    rect.x = x-w/2;
    rect.y = y-h/2;
}

void House::processFont(sttfont_memory& mem, SDL_Renderer* renderer, std::array<uint8_t, 3> prices) {
        balance.fc.faceSize = 22;
        balance.fc.loadFontManaged(mem);
        balance.fc.bindRenderer(renderer);

        progress.fc.faceSize = 22;
        progress.fc.loadFontManaged(mem);
        progress.fc.bindRenderer(renderer);

        no_money.fc.faceSize = 18;
        no_money.fc.loadFontManaged(mem);
        no_money.fc.bindRenderer(renderer);
        std::string no_money_label = "   NOT   \nENOUGHT\n  MONEY";
        SDL_DestroyTexture(no_money.texture);
        no_money.texture = no_money.fc.renderTextToTexture(no_money_label.c_str(),
                                                                 no_money_label.size(),
                                                                 &no_money.endTexturePoint.x,
                                                                 &no_money.endTexturePoint.y);

        price_small.fc.faceSize = 18;
        price_small.fc.loadFontManaged(mem);
        price_small.fc.bindRenderer(renderer);
        std::string price_small_str = std::to_string(prices.at(0));
        SDL_DestroyTexture(price_small.texture);
        price_small.texture = price_small.fc.renderTextToTexture(price_small_str.c_str(),
                                                                 price_small_str.size(),
                                                                 &price_small.endTexturePoint.x,
                                                                 &price_small.endTexturePoint.y);

        price_med.fc.faceSize = 18;
        price_med.fc.loadFontManaged(mem);
        price_med.fc.bindRenderer(renderer);
        std::string price_med_str = std::to_string(prices.at(1));
        SDL_DestroyTexture(price_med.texture);
        price_med.texture = price_med.fc.renderTextToTexture(price_med_str.c_str(),
                                                             price_med_str.size(),
                                                             &price_med.endTexturePoint.x,
                                                             &price_med.endTexturePoint.y);

        price_large.fc.faceSize = 18;
        price_large.fc.loadFontManaged(mem);
        price_large.fc.bindRenderer(renderer);
        std::string price_large_str = std::to_string(prices.at(2));
        SDL_DestroyTexture(price_large.texture);
        price_large.texture = price_large.fc.renderTextToTexture(price_large_str.c_str(),
                                                                 price_large_str.size(),
                                                                 &price_large.endTexturePoint.x,
                                                                 &price_large.endTexturePoint.y);
}

SDL_Rect* House::getRectTexture_balance () {
        balance.rectTexture = {rect.x+rect.w/12, rect.y+rect.h/20,
                               balance.endTexturePoint.x, balance.endTexturePoint.y};
        if (balance.rectTexture.w>rect.w/3) balance.rectTexture.w = rect.w/3;
        return &balance.rectTexture;
}

SDL_Rect* House::getRectProgress_value () {
        progress.rectTexture = {static_cast<int>(rect.x+rect.w*0.65), rect.y+rect.h/20,
                               progress.endTexturePoint.x, progress.endTexturePoint.y};
        return &progress.rectTexture;
}

SDL_Texture* House::getTexture_balance () {
        SDL_SetTextureColorMod(balance.texture, 0,0,0);
        return balance.texture;
}

SDL_Texture* House::getProgress_value () {
        SDL_SetTextureColorMod(progress.texture, 0,0,0);
        return progress.texture;
}

SDL_Rect* House::getRectTexture_noMoney () {
        no_money.rectTexture = {static_cast<int>(rect.x+rect.w*0.28f),
                                static_cast<int>(rect.y+rect.h*0.2f),
                                no_money.endTexturePoint.x, no_money.endTexturePoint.y};
        return &no_money.rectTexture;
}

SDL_Texture* House::getTexture_noMoney () {
    SDL_SetTextureColorMod(no_money.texture, 0,0,0);
    return no_money.texture;
}

void House::updateBalance (uint32_t balance_value) {

    std::string balance_str = std::to_string(balance_value);
    SDL_DestroyTexture(balance.texture);
    balance.texture = balance.fc.renderTextToTexture(balance_str.c_str(),
                                                     balance_str.size(),
                                                     &balance.endTexturePoint.x,
                                                     &balance.endTexturePoint.y);
}

void House::updateProgress (double progress_value) {

    std::string progress_str = std::to_string(static_cast<int>(progress_value*100.0)) + "%";
    SDL_DestroyTexture(progress.texture);
    progress.texture = progress.fc.renderTextToTexture(progress_str.c_str(),
                                                     progress_str.size(),
                                                     &progress.endTexturePoint.x,
                                                     &progress.endTexturePoint.y);
}

SDL_Rect* House::getRectPriceTexture (UnitSize size) {
    int y = static_cast<int>(rect.y+rect.h*0.85f);
    int x = rect.x;
    int w, h;
    switch (size) {
    case SMALL:
        x += rect.w*0.12f;
        w = price_small.endTexturePoint.x;
        h = price_small.endTexturePoint.y;
        price_small.rectTexture  = {x,y,w,h};
        return &price_small.rectTexture;
    case MEDIUM:
        x += rect.w*0.42f;
        w = price_med.endTexturePoint.x;
        h = price_med.endTexturePoint.y;
        price_med.rectTexture  = {x,y,w,h};
        return &price_med.rectTexture;
    case LARGE:
        x += rect.w*0.75f;
        w = price_large.endTexturePoint.x;
        h = price_large.endTexturePoint.y;
        price_large.rectTexture  = {x,y,w,h};
        return &price_large.rectTexture;
    }
}

SDL_Texture* House::getPriceTexture (UnitSize size) {
    switch (size) {
        case SMALL:
            SDL_SetTextureColorMod(price_small.texture, 0,0,0);
            return price_small.texture;
        case MEDIUM:
            SDL_SetTextureColorMod(price_med.texture, 0,0,0);
            return price_med.texture;
        case LARGE:
            SDL_SetTextureColorMod(price_large.texture, 0,0,0);
            return price_large.texture;
    }
}
int House::getDiamondIndexSize () const {
    return lastFocused.has_value() ? DiamondindexListFocused.size() : DiamondindexListUnfocused.size();
}

const int* House::getDiamondIndexList () {
    return lastFocused.has_value() ? DiamondindexListFocused.data() : DiamondindexListUnfocused.data();
}

SDL_Vertex* House::getDiamondVerticies () {
    DiamondVerticies = {
                 {{static_cast<float>(rect.x+rect.w*0.05f),static_cast<float>(rect.y+rect.h*0.37f)},{255,255,255,255}},
                 {{static_cast<float>(rect.x+rect.w/2), static_cast<float>(rect.y+rect.h*0.15f)},{255,255,255,255}},
                 {{static_cast<float>(rect.x+rect.w*0.95f),static_cast<float>(rect.y+rect.h*0.37f)},{255,255,255,255}},
                 {{static_cast<float>(rect.x+rect.w/2),static_cast<float>(rect.y+rect.h*0.6f)},{255,255,255,255}},
                 {{static_cast<float>(rect.x+rect.w*0.15f),static_cast<float>(rect.y+rect.h*0.37f)},{255,255,255,255}},
                 {{static_cast<float>(rect.x+rect.w/2), static_cast<float>(rect.y+rect.h*0.2f)},{255,255,255,255}},
                 {{static_cast<float>(rect.x+rect.w*0.85f),static_cast<float>(rect.y+rect.h*0.37f)},{255,255,255,255}},
                 {{static_cast<float>(rect.x+rect.w/2),static_cast<float>(rect.y+rect.h*0.55f)},{255,255,255,255}},
                 };
    return DiamondVerticies.data();
}

SDL_Rect* House::getSlot_Rect (UnitSize size) {
    int w = rect.w*0.26f;
    int h = rect.h*0.31f;
    int y = rect.y+rect.h*0.67f;
    int x = rect.x;
    switch (size) {
    case SMALL:
        x += rect.w*0.06f;
        small_slot = {x,y,w,h};
        return &small_slot;
    case MEDIUM:
        x += rect.w*0.37f;
        med_slot = {x,y,w,h};
        return &med_slot;
    case LARGE:
        x += rect.w*0.68f;
        large_slot = {x,y,w,h};
        return &large_slot;
    }
}

SDL_Rect* House::getSample_Rect (UnitSize size) {
    int x,y,w,h;
    switch (size) {
    case SMALL:
        x = small_slot.x + small_slot.w*0.4f;
        y = small_slot.y + small_slot.h*0.2f;
        w = small_slot.w*0.2f;
        h = small_slot.w*0.4f;
        small_sample = {x,y,w,h};
        return &small_sample;
    case MEDIUM:
        x = med_slot.x + med_slot.w*0.18f;
        y = med_slot.y + med_slot.h*0.16f;
        w = med_slot.w*0.7f;
        h = med_slot.w*0.5f;
        med_sample = {x,y,w,h};
        return &med_sample;
    case LARGE:
        x = large_slot.x + large_slot.w*0.12f;
        y = large_slot.y + large_slot.h*0.1f;
        w = large_slot.w*0.78f;
        h = w;
        large_sample = {x,y,w,h};
        return &large_sample;
    }
}

bool House::isPointInsideSlots (SDL_Point point) const {
    if (SDL_PointInRect(&point, &small_slot)) {
        lastFocused = SMALL;
        return true;
    } else if (SDL_PointInRect(&point, &med_slot)) {
        lastFocused = MEDIUM;
        return true;
    } else if (SDL_PointInRect(&point, &large_slot)) {
        lastFocused = LARGE;
        return true;
    }
    lastFocused = {};
    return false;
}

SDL_Point House::getDiamondMiddlePoint () const {
    return {rect.x+rect.w/2, static_cast<int>(DiamondVerticies.front().position.y)};
}



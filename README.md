# Template for the Future Game (PLAYABLE, but NOT FULLY READY!)

!!! Live Demo will be added soon !!!<br/>
The project is using [Victor Stone's](https://github.com/SandSnip3r) pathfinder [library](https://github.com/SandSnip3r/Pathfinder)<br/>
For rendering font  - [SDL STB Font Renderer](https://github.com/SnapperTT/sdl_stb_font) (a header-only library for rendering text - included in this project)

### Dependencies

Pure SDL2 and TBB (for multithreading).
A compiler with C++20 support is required.
You can install dependencies with following commands (Debian/Ubuntu):
```bash
sudo apt install libsdl2-dev cmake g++ libtbb-dev
```
### Usage and Game Rules

You can buy new objects in the main base <br/>
There are 3 types of objects: Small (fast and weak), Medium (more health, but slower) and Large (a lot of health, but very slow)<br/>
![Alt text](examples/base_image.png)

Collect diamonds to get money!<br/>
Diamonds appear randomly. The more objects you have, the greater the chance of diamonds appearing on the map.<br/>
Dodge bombs (black dot for a second, then explosion). <br/>
![Alt text](examples/bomb_image.png)
Epicenter causes more damage.<br/>

To win the game reach 100% (creating the Large object add more %)<br/>

 
### Building

The code can be built using cmake (tested on Linux)
```bash
git clone https://codeberg.org/Higgzes/SimpleSDL2_Game_Template.git
cd SimpleSDL2_Game_Template
```
To clone Pathfinder implementation 
```bash
cd include && git clone https://github.com/SandSnip3r/Pathfinder.git && cd ..
```
Build and run
```bash
mkdir build && cd build
cmake ..
make
./game
```
### TODO
1. Increase Performance
2. Handle and process library errors 
3. Friendly UI
4. Multi-threading safe

### Example

![Alt text](examples/sample.gif?raw=true "Game")
